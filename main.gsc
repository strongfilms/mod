#include common_scripts\utility;
#include maps\mp\zombies\_zm;
#include maps\mp\zombies\_zm_utility;
#include maps\mp\gametypes_zm\_hud_util;
#include maps\mp\gametypes_zm\_hud_message;
#include maps\mp\_utility;
#include common_scripts\utility;

init(){
	level.continuar = 0;
	level.clientid = 0;
	level.perk_purchase_limit = 9;
	level thread onplayerconnect();
	drawZombiesCounter();
	//thread checkPause();
}

checkPause(){
	for (;;){
		self iprintln("Hola");
		if(level MeleeButtonPressed()){
			self iprintln("Acuchillado");
		}
	}
}

checkBtns(){
	self endon("disconnect");
	for(;;){
		wait 0.05;
		secondsPressed = 0;
		while(self meleeButtonPressed()){
			if(self meleeButtonPressed()){
				secondsPressed = secondsPressed + 1;
				if(secondsPressed == 3){
					self iprintln("Inmune desactivado");
					level.continuar = 0;
				}
				else if(secondsPressed > 3){
				}
				else{
					self iprintln("Desactivando inmune en " + (3-(secondsPressed)) + "...");
					weapon = self getcurrentweapon(); //Arma actual
					kills = level.zombie_player_killed_count; //Zombie kills totales
				}
				wait 1;
			}
		}
	}
}

drawKilledZombies(){
	self endon("disconnect");
	oldZmKills = 0;
	oldWaveNumber = 1;
	while(1){

		newWaveNumber = int(level.round_number);

		if(oldWaveNumber != newWaveNumber){

			newZmKills = level.zombie_player_killed_count;

			if(oldZmKills != newZmKills){

				self iprintln("Zombies eliminados en esta ronda: ^1" + (newZmKills-oldZmKills));

				oldZmKills = newZmKills;
				oldWaveNumber = newWaveNumber;

			}

		}
		wait 2;
	}

}

/*beInmune(){
	continue_loop = 0;
	self endon("disconnect");
	for(;;){ //Comprobar si se pusa el boton cada 1 segundo
		wait 1;
		while(self SecondaryOffhandButtonPressed()){
			if(self SecondaryOffhandButtonPressed()){

				if(continue_loop == 0){ //primera ejecucion
					
					bighealth = 10000000;
					continue_loop = 1;
					while(continue_loop == 1){
						if( self.health < bighealth){
							self.health = bighealth;
							self iprintln("God mode activado");
						}
					}

				}else{
					continue_loop = 0;
					self.health = 250;
					self iprintln(self.health);
					break;
				}
				wait 1;
			}
		}
		
	}
}*/

beInmune(){
	toggle_god_mode = 0;
	self endon("disconnect");
	for(;;){
		wait 1;
		while(self SecondaryOffhandButtonPressed()){
			if(self SecondaryOffhandButtonPressed()){
					
					bighealth = 10000000;

					if(toggle_god_mode == 0){
						toggle_god_mode = 1;
						self maps/mp/zombies/_zm_perks::give_perk( "specialty_armorvest" );
						self iprintln("Inmune activado");
						level.continuar = 1;
					}else{
						toggle_god_mode = 0;
					}
					
					while(level.continuar == 1){

						if(toggle_god_mode == 1){ //primera ejecucion
							self.health = 10000000;
						}
						wait 0.05;
					}

				wait 1;
			}
			wait 1;
		}
	}
}

onplayerconnect(){ 
	for (;;){
		level waittill("connecting", player);
		player.clientid = level.clientid;
		player thread onplayerspawned();
		level.clientid++;
	}
}

onplayerspawned(){

	self endon("disconnect");
		level endon("game_ended");
		//self thread AnimatedTextCUSTOMPOS("^1Origins con los meus \n^7Prohibido ^5viciar", 0,-200); //Welcome Messages
		self thread AnimatedTextCUSTOMPOS("^7Bienvenido " + self.name, 0,-200);
		for(;;){
			self waittill("spawned_player");
			if(level.round_number > 5)
				self.score = self.score + (int(level.round_number/5)*1000);
			if(self.donator)
				self.score = self.score;
		}
		
}

AnimatedTextCUSTOMPOS(text, x, y){ //Made by DoktorSAS
	textSubStr = getSubStr(text,0,text.size);
	result = "";
	welcome = self createFontString("hudsmall",1.9);
	welcome setPoint("CENTER","CENTER",x, y);
	welcome setText("");	
	for(i=0;i<textSubStr.size;i++){
		color = textSubStr[i]+textSubStr[i+1];
		if(color == "^1" || color == "^2" || color == "^3" || color == "^4" || color == "^5" || color == "^6" || color == "^7" || color == "^8" || color == "^0" || color == "\n"){
			result = result + color;
			i++;
		}else
			result = result + textSubStr[i];
		if(i == textSubStr.size){
			welcome setText(text);
		}else{
			welcome setText(result);
			wait 0.1;
			welcome setText(result + "^7_");
		}
		wait 0.1;
	}
	wait 2;
	welcome setText("");
	//self iprintln("Whats Inside: \n # ^1NO ^2Perk ^7Limit \n # ^1Zombies ^7Counter");
	thread checkBtns();
	thread beInmune();
	thread drawKilledZombies();
	thread drawHealthBar();
	/*thread beNotInmune(stop_bucle);*/
}

drawZombiesCounter(){ //Thanks to CabConModding
    level.zombiesCountDisplay = createServerFontString("hudsmall" , 1.9);
    level.zombiesCountDisplay setPoint("CENTER", "CENTER", "CENTER", 200);
    thread updateZombiesCounter();
}
updateZombiesCounter(){ //Thanks to CabConModding
    level endon("stopUpdatingZombiesCounter");
    while(true){
        zombiesCount = get_current_zombie_count();
        if(zombiesCount >= 0){
        	level.zombiesCountDisplay setText("Zombies: ^1" + zombiesCount);
        }else
        	level.zombiesCountDisplay setText("Zombies: ^2" + zombiesCount);
        waitForZombieCountChanged("stopUpdatingZombiesCounter");
    }
}
recreateZombiesCounter(){ //Thanks to CabConModding
    level notify("stopUpdatingZombiesCounter");
    thread updateZombiesCounter();
}
waitForZombieCountChanged(endonNotification){ //Thanks to CabConModding
    level endon(endonNotification);
    oldZombiesCount = get_current_zombie_count();
    while(true){
        newZombiesCount = get_current_zombie_count();
        if(oldZombiesCount != newZombiesCount){
            return;
        }
        wait 0.05;
    }
}

drawHealthBar(){
	self endon("death");
	self.ProcessBar2=createPrimaryProgressBar();
	for(i=0;i<101;i++){
		//self.ProcessBar2 updateBar(i / 100);
		//self.CreateText setValue(i);
		self.ProcessBar2 setPoint("CENTER","CENTER",200,195);
		self.ProcessBar2.color=(0, 0, 0);
		self.ProcessBar2.bar.color=(0, 0, 0);
		self.ProcessBar2.alpha=1;

		while(true){
			currentHealth = self.health;

			if(currentHealth > 300){
				self.ProcessBar2 updateBar(1);

				while(self.health > 300){
				
					self.ProcessBar2.bar.color=(randomint(255)/255, randomint(255)/255, randomint(255)/255);

					wait 0.05;
				}

			}else{

				if(currentHealth < 50){
					self.ProcessBar2.bar.color=(219/255, 26/255, 0);
					self.ProcessBar2 updateBar(currentHealth / 250);
				} else{
					self.ProcessBar2.bar.color=(1, 1, 1);
					self.ProcessBar2 updateBar(currentHealth / 250);
				}

			}
        	wait 0.05;
    	}

		wait .1;
	}
}

